{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DerivingVia     #-}
{-# LANGUAGE NamedFieldPuns  #-}
{-# LANGUAGE RecordWildCards #-}

module GuchiTx.Lend.OffChain where

import Data.Text               ( pack )

import Ledger.Value            ( Value (Value) )

import PlutusTx.AssocMap       qualified as AssocMap


import GuchiTx.StateMachine    ( MachineTransition (mtMachineWithState) )
import GuchiTx.StateMachine    qualified as SM

import GuchiTx.Common.OffChain
import GuchiTx.Common.OnTypes
import GuchiTx.Lend.OffTypes
import GuchiTx.Lend.OnTypes
import GuchiTx.Types
    ( CurrentState (CurrentState, csMachineParams, csStateParams, csTxOutRef)
    , Request (..)
    , Response (..)
    , TxError (TransitionError, UtxError)
    )
import Ledger.Ada              ( lovelaceValueOf )


initTx :: InitCParams -> PrereqParams -> Either TxError LendResponse
initTx icp@InitCParams{..} pp@PrereqParams{ppMinAdaBuffer} =
    let
        ip = ipFromIcp icp
        tt = SM.mkThreadToken (fst icpUtxo)
        assets = Value (AssocMap.singleton icpCurrencySymbol (AssocMap.fromList icpAmounts)) <> lovelaceValueOf ppMinAdaBuffer
        trans = SM.initMachine icpUtxo (mkMachineWithMStake $ LendParams pp ip tt) Initted assets Nothing
        lp = LendParams pp ip tt
        utxM = SM.transTx trans
    in case utxM of
            Left e    -> Left (UtxError $ pack $ show e)
            Right utx -> Right $ Response lp (SM.State Initted assets) (modTxOut (modStakeHack isScript (ppSkh pp)) utx)

stepTx :: LendRequest -> Either TxError LendResponse
stepTx Request{..} =
    let CurrentState{..} = rqCurrent
        mtM = SM.mkStep (mkMachineWithUtxo csTxOutRef csStateParams csMachineParams) rqInput
        LendParams pp _ _ = csMachineParams
    in case mtM of
            Left e -> Left  (TransitionError $ pack $ show e)
            Right mt -> do
                case SM.transTx mt of
                     Left e    -> Left (UtxError $ pack $ show e)
                     Right utx -> Right $ Response csMachineParams (SM.mwsState $ mtMachineWithState mt)  (modTxOut (modStakeHack isScript (ppSkh pp)) utx)

