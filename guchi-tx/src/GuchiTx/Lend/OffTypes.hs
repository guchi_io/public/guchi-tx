{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards    #-}

module GuchiTx.Lend.OffTypes where

import Data.Aeson              ( FromJSON, ToJSON )
import GHC.Generics            ( Generic )

import Ledger                  qualified as L
import Ledger.Ada              qualified as Ada

import GuchiTx.Common.OffChain ( skhToCredential )
import GuchiTx.Common.OnTypes
import GuchiTx.Lend.OnTypes
import GuchiTx.Lend.Validator  ( mkInstance )
import GuchiTx.StateMachine    qualified as SM
import GuchiTx.Types

data InitCParams
  = InitCParams
      { icpCurrencySymbol :: !L.CurrencySymbol
      , icpAmounts        :: ![(L.TokenName, Integer)]
      , icpDuration       :: !L.POSIXTime
      , icpCollateral     :: !Integer
      , icpFee            :: !Integer
      , icpReturnPkh      :: !L.PaymentPubKeyHash
      , icpReturnSkh      :: !(Maybe L.StakePubKeyHash)
      , icpPkh            :: !L.PaymentPubKeyHash
      , icpUtxo           :: !(L.TxOutRef, L.ChainIndexTxOut)
      }
  deriving (Eq, FromJSON, Generic, Show, ToJSON)

ipFromIcp :: InitCParams -> InitParams
ipFromIcp InitCParams{..} = InitParams { ipLockedCs = LockedCs icpCurrencySymbol
                           , ipAmount = LockedAmount (foldl (\c (_, n) ->  c + n) 0 icpAmounts)
                           , ipDuration = icpDuration
                           , ipCollateral = Collateral (Ada.lovelaceOf icpCollateral)
                           , ipLenderFee = LenderFee (Ada.lovelaceOf icpFee)
                           , ipReturnPkh = icpReturnPkh
                           , ipReturnSkh = icpReturnSkh
                           , ipParty = icpPkh
                           }


data LendParams
  = LendParams
      { lpPrereqParams :: PrereqParams
      , lpInitParams   :: InitParams
      , lpThreadToken  :: SM.ThreadToken
      }
  deriving (FromJSON, Generic, Show, ToJSON)


type LendCurrent = CurrentState LendParams LendState

type LendRequest = Request LendParams LendState LendInput

type LendResponse = Response LendParams LendState

mkMachineWithMStake :: LendParams -> SM.MachineWithMStake LendState LendInput
mkMachineWithMStake (LendParams pp ip tt) = SM.MachineWithMStake (mkInstance pp ip tt) (fmap skhToCredential $ ppSkh pp)

mkMachineWithUtxo :: L.TxOutRef -> SM.State LendState -> LendParams -> SM.MachineWithUtxo LendState LendInput
mkMachineWithUtxo tor s lp = SM.mwmsAddUtxo tor s (mkMachineWithMStake lp)
