{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE TemplateHaskell    #-}

{-# OPTIONS_GHC -fno-specialise #-}

module GuchiTx.Lend.OnTypes where

import Data.Aeson             ( FromJSON, ToJSON )
import GHC.Generics           ( Generic )
import GuchiTx.Common.OnTypes ( Collateral, LenderFee, LockedAmount, LockedCs )
import GuchiTx.StateMachine   ( StateMachine )
import Ledger                 qualified as L
import PlutusTx qualified
import PlutusTx.Eq qualified
import PlutusTx.Prelude       ( Bool (..), Eq ((==)), Integer, Maybe, (&&) )
import Prelude                qualified as Haskell

-- | Parameters for specifying a guchi state machine.
data InitParams
  = InitParams
      { ipLockedCs   :: LockedCs
      , ipAmount     :: LockedAmount
      , ipCollateral :: Collateral
      , ipDuration   :: L.POSIXTime
      , ipLenderFee  :: LenderFee
      , ipReturnPkh  :: L.PaymentPubKeyHash
      , ipReturnSkh  :: Maybe L.StakePubKeyHash
      , ipParty      :: L.PaymentPubKeyHash
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''InitParams

-- | STATE

data EngagedState
  = EngagedState
      { esPkh :: L.PaymentPubKeyHash
      , esNow :: L.POSIXTime
        -- ^ to be within 3 hours of validators 'now'.
      }
  --deriving stock (Haskell.Show, Haskell.Ord, Haskell.Eq, Generic)
  deriving (Generic)
  deriving (Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''EngagedState
PlutusTx.unstableMakeIsData ''EngagedState

instance PlutusTx.Eq.Eq EngagedState where
    {-# INLINABLE (==) #-}
    EngagedState aPkh aNow == EngagedState bPkh bNow = aPkh ==  bPkh && aNow == bNow

-- | The states of the guchi state machine.
data LendState
  = Initted
  | Engaged EngagedState
  | Canceled
  | Returned
  | Claimed
  --deriving stock (Haskell.Show, Generic)
  deriving (Generic)
  deriving (Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

instance PlutusTx.Eq.Eq LendState where
    {-# INLINABLE (==) #-}
    Initted == Initted        = True
    Engaged ep == Engaged ep' = ep == ep'
    Canceled == Canceled      = True
    Returned == Returned      = True
    Claimed == Claimed        = True
    _ == _                    = False

PlutusTx.makeLift ''LendState
PlutusTx.unstableMakeIsData ''LendState

-- END OF STATE

-- TRANSITIONS

-- | TRANSITION PARAMS

data EngageParams
  = EngageParams
      { epPkh :: L.PaymentPubKeyHash
        -- ^ Bob's pkh
      , epNow :: L.POSIXTime
        -- ^ to be within 3 hours of validators 'now'.
      }
  --deriving stock (Haskell.Show, Haskell.Ord, Haskell.Eq, Generic)
  deriving (Generic)
  deriving (Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''EngageParams
PlutusTx.unstableMakeIsData ''EngageParams

data ReturnParams
  = ReturnParams
      { rpAmounts :: [(L.TokenName, Integer)]
        -- ^Bob sends alice.
        -- This is Logically redundant, but simplifies the encoding of the condition as a constraint.
      }
  --deriving stock (Haskell.Show, Haskell.Eq, Generic)
  deriving (Generic)
  deriving (Haskell.Eq, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)


PlutusTx.makeLift ''ReturnParams
PlutusTx.unstableMakeIsData ''ReturnParams

data LendInput
  = Engage EngageParams
  | Return ReturnParams
  | Cancel
  | Claim
  deriving (Generic)
  deriving (Haskell.Eq, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)


PlutusTx.makeLift ''LendInput
PlutusTx.unstableMakeIsData ''LendInput

type LendMachine = StateMachine LendState LendInput
