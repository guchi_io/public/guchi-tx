{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeApplications   #-}

module GuchiTx.Lend.Validator where

import Data.Void                            ( Void )

import Plutus.Script.Utils.V1.Typed.Scripts ( mkUntypedValidator, validatorAddress )
import PlutusTx qualified
import PlutusTx.Prelude
    ( Bool (False, True)
    , Maybe (Just, Nothing)
    , id
    , maybe
    , mempty
    , traceError
    , traceIfFalse
    , ($)
    , (&&)
    , (*)
    , (+)
    , (.)
    , (<>)
    , (==)
    , (>)
    , (>=)
    )

import Ledger                               qualified as L
import Ledger.Ada                           ( getLovelace )
import Ledger.Ada                           qualified as Ada
import Ledger.Constraints                   qualified as Constraints
import Ledger.Constraints.TxConstraints     ( TxConstraints )
import Ledger.Interval                      qualified as Interval
import Ledger.Typed.Scripts                 qualified as Scripts
import Ledger.Value                         ( Value )

import GuchiTx.Common.OnChain
import GuchiTx.Common.OnTypes
import GuchiTx.Lend.OnTypes
import GuchiTx.StateMachine                 ( State (..), StateMachine (..) )
import GuchiTx.StateMachine                 qualified as SM

-- | TRANSITION FUNCTIONS

engageTr
    :: PrereqParams
    -> InitParams
    -> EngageParams
    -> Value
    -> Maybe (TxConstraints Void Void, State LendState)
engageTr pp@PrereqParams{..} InitParams{..} ep _cVal = Just (constraints, newState)
    where
        constraints = mustPayLenderFee
                  <> mustValidateSoon (epNow ep)

        mustPayLenderFee = mustPayAddress ipReturnPkh ipReturnSkh (Ada.toValue (unLenderFee ipLenderFee))

        pFee = calcPFee pp ipCollateral
        lockedCollateral = Ada.toValue (unCollateral ipCollateral + Ada.lovelaceOf ppMinAdaBuffer) <> pFee
        newState = State (Engaged $ EngagedState { esPkh = epPkh ep, esNow = epNow ep}) lockedCollateral

returnTr
    :: PrereqParams
    -> InitParams
    -> ReturnParams
    -> EngagedState
    -> Maybe (TxConstraints Void Void, State LendState)
returnTr pp@PrereqParams{..} InitParams{..} rp es = if isValidAssets then Just (constraints, newState) else Nothing
    where
        isValidAssets = totalCurrencySymbol (unLockedCs ipLockedCs) obligationValue == unLockedAmount ipAmount

        constraints = mustBeSignedByCounterParty
                  <> mustPayLenderObligation
                  <> mustPayPFee
                  <> mustBeBeforeDeadline

        mustBeSignedByCounterParty =  Constraints.mustBeSignedBy (esPkh es)
        mustPayLenderObligation = mustPayAddress ipReturnPkh ipReturnSkh obligationValue
        mustBeBeforeDeadline =  Constraints.mustValidateIn (Interval.to $ ipDuration + esNow es)
        mustPayPFee = mustPayAddress ppPkh ppSkh pFee

        pFee = calcPFee pp ipCollateral
        obligationValue = csAmountsToValue (unLockedCs ipLockedCs) (rpAmounts rp) <> Ada.lovelaceValueOf ppMinAdaBuffer
        newState = State Returned mempty

{-# INLINABLE transition #-}
transition
    :: PrereqParams
    -> InitParams
    -> State LendState
    -> LendInput
    -> Maybe (TxConstraints Void Void, State LendState)
transition pp ip (State cState cVal) tr =
    case (tr, cState) of
         (Cancel, Initted)       -> cancelTr pp (ipParty ip) Canceled
         (Engage ep, Initted)    -> engageTr pp ip ep cVal
         (Return up, Engaged es) -> returnTr pp ip up es
         (Claim, Engaged es)     -> claimTr  pp (ipParty ip) (esNow es + ipDuration ip) (calcPFee pp $ ipCollateral ip) Claimed
         _                       -> Nothing


-- END OF TRANSITION FUNCTIONS

{-# INLINABLE check #-}
check :: PrereqParams -> InitParams -> LendState -> LendInput -> L.ScriptContext -> Bool
check PrereqParams{..} ip _s (Engage _sp) ctx =
    traceIfFalse "C0" (isValidStake ppSkh ownStakePkh)
    && traceIfFalse "C1" isValidAssets
    && traceIfFalse "C2" isValidAda
    && traceIfFalse "C3" isValidDuration
        where
            ownStakePkh = toStakePkh . L.txOutAddress . L.txInInfoResolved $ ownInput
            isValidAssets = totalCurrencySymbol (unLockedCs $ ipLockedCs ip) ownVal == unLockedAmount (ipAmount ip)
            isValidAda = getLovelace (Ada.fromValue ownVal) >= ppMinAdaBuffer
            isValidDuration = L.getPOSIXTime (ipDuration ip) > 3 * 60 * 60 * 1000  -- 3 hours

            ownInput :: L.TxInInfo
            ownInput = maybe (traceError "C10" {-"Can't find validation input"-}) id (L.findOwnInput ctx)
            ownVal = (L.txOutValue . L.txInInfoResolved) ownInput
check _ _ _ _ _ = True

isFinal :: LendState -> Bool
isFinal Canceled = True
isFinal Returned = True
isFinal Claimed  = True
isFinal _        = False

-- BOILERPLATE

{-# INLINABLE stateMachine #-}
stateMachine :: PrereqParams -> InitParams -> SM.ThreadToken-> LendMachine
stateMachine pp ip tt =
    SM.StateMachine { smTransition = transition pp ip
                    , smCheck = check pp ip
                    , smThreadToken = Just tt
                    , smFinal = isFinal
                    }

{-# INLINABLE mkValidator #-}
mkValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> LendState -> LendInput -> L.ScriptContext -> Bool
mkValidator pp ip tt = SM.mkValidator (stateMachine pp ip tt)

tValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> Scripts.TypedValidator LendMachine
tValidator pp ip tt = Scripts.mkTypedValidator @LendMachine
        ($$(PlutusTx.compile [|| mkValidator ||])
        `PlutusTx.applyCode` PlutusTx.liftCode pp
        `PlutusTx.applyCode` PlutusTx.liftCode ip
        `PlutusTx.applyCode` PlutusTx.liftCode tt
        )
        $$(PlutusTx.compile [|| wrap ||])
     where
         wrap = mkUntypedValidator @LendState @LendInput

-- type Client = SM.StateMachineClient LendState LendInput

mkInstance :: PrereqParams -> InitParams -> SM.ThreadToken -> SM.StateMachineInstance LendState LendInput
mkInstance pp ip tt = SM.StateMachineInstance (stateMachine pp ip tt) (tValidator pp ip tt)

validator :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Validator
validator pp ip tt = Scripts.validatorScript  (tValidator pp ip tt)

address :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Address
address pp ip tt = validatorAddress $ tValidator pp ip tt
