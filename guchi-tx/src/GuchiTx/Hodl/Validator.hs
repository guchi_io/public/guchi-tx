{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeApplications   #-}



module GuchiTx.Hodl.Validator where

import Data.Void                            ( Void )
import GuchiTx.Common.OnChain
    ( calcPFee
    , cancelTr
    , csAmountsToValue
    , isValidStake
    , mustPayAddress
    , toStakePkh
    , totalCurrencySymbol
    )
import GuchiTx.Common.OnTypes
    ( Collateral (unCollateral)
    , LockedAmount (unLockedAmount)
    , LockedCs (unLockedCs)
    , PrereqParams (..)
    )
import GuchiTx.Hodl.OnTypes
    ( ClaimParams (ClaimParams)
    , HodlInput (..)
    , HodlMachine
    , HodlState (..)
    , InitParams (..)
    )
import GuchiTx.StateMachine.OnChain         ( State (..), StateMachine (..) )
import GuchiTx.StateMachine.OnChain         qualified as SM
    ( StateMachine (StateMachine)
    , StateMachineInstance (StateMachineInstance)
    , mkValidator
    )
import GuchiTx.StateMachine.ThreadToken     qualified as SM ( ThreadToken )
import Ledger                               qualified as L
import Ledger.Ada                           ( getLovelace )
import Ledger.Ada                           qualified as Ada
import Ledger.Constraints.TxConstraints     ( TxConstraints )
import Ledger.Typed.Scripts                 qualified as Scripts
import Plutus.Script.Utils.V1.Typed.Scripts ( mkUntypedValidator )
import PlutusTx qualified
import PlutusTx.Prelude
    ( Bool (False, True)
    , Maybe (Just, Nothing)
    , id
    , maybe
    , mempty
    , traceError
    , traceIfFalse
    , ($)
    , (&&)
    , (+)
    , (.)
    , (<>)
    , (==)
    , (>=)
    )

-- | TRANSITION FUNCTIONS

claimTr
    :: PrereqParams
    -> InitParams
    -> ClaimParams
    -> Maybe (TxConstraints Void Void, State HodlState)
claimTr pp@PrereqParams{..} InitParams{..} (ClaimParams amts) =
    if isValidAssets
        then Just (constraints, newState)
        else Nothing
    where
        isValidAssets = totalCurrencySymbol (unLockedCs ipLockedCs) val2Party == unLockedAmount ipAmount

        constraints = mustPayParty <> mustPayPFee
        mustPayParty = mustPayAddress ipReturnPkh ipReturnSkh val2Party
        mustPayPFee = mustPayAddress ppPkh ppSkh pFee

        val2Party = csAmountsToValue (unLockedCs ipLockedCs) amts <> Ada.lovelaceValueOf ppMinAdaBuffer
        pFee = calcPFee pp ipCollateral
        newState = State Claimed mempty

{-# INLINABLE transition #-}
transition
    :: PrereqParams
    -> InitParams
    -> State HodlState
    -> HodlInput
    -> Maybe (TxConstraints Void Void, State HodlState)
transition pp ip (State cState _) tr =
    case (tr, cState) of
         (Cancel, Initted)   -> cancelTr pp (ipParty ip) Canceled
         (Claim cp, Initted) -> claimTr  pp ip cp
         _                   -> Nothing


-- END OF TRANSITION FUNCTIONS

{-# INLINEABLE initValue #-}
initValue :: PrereqParams -> InitParams -> L.Value
initValue PrereqParams{..} InitParams{..} = Ada.lovelaceValueOf $ (getLovelace $ unCollateral ipCollateral) + ppMinAdaBuffer

{-# INLINABLE check #-}
check :: PrereqParams -> InitParams -> HodlState -> HodlInput -> L.ScriptContext -> Bool
check pp@PrereqParams{ppSkh} ip _s (Claim _) ctx =
    traceIfFalse "C0" (isValidStake ppSkh ownStakePkh)
    && traceIfFalse "C2" isValidAda
        where
            isValidAda = getLovelace (Ada.fromValue ownVal) >= getLovelace (Ada.fromValue $ initValue pp ip)
            ownInput = maybe (traceError "C10" {-"Can't find validation input"-}) id (L.findOwnInput ctx)
            ownVal = L.txOutValue . L.txInInfoResolved $ ownInput
            ownStakePkh = toStakePkh . L.txOutAddress . L.txInInfoResolved $ ownInput
check _ _ _ _ _ = True

isFinal :: HodlState -> Bool
isFinal Canceled = True
isFinal Claimed  = True
isFinal _        = False

-- BOILERPLATE

{-# INLINABLE stateMachine #-}
stateMachine :: PrereqParams -> InitParams -> SM.ThreadToken -> HodlMachine
stateMachine pp ip tt =
    SM.StateMachine { smTransition = transition pp ip
                    , smCheck = check pp ip
                    , smThreadToken = Just tt
                    , smFinal = isFinal
                    }

{-# INLINABLE mkValidator #-}
mkValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> HodlState -> HodlInput -> L.ScriptContext -> Bool
mkValidator pp ip tt = SM.mkValidator (stateMachine pp ip tt)

tValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> Scripts.TypedValidator HodlMachine
tValidator pp ip tt = Scripts.mkTypedValidator @HodlMachine
        ($$(PlutusTx.compile [|| mkValidator ||])
        `PlutusTx.applyCode` PlutusTx.liftCode pp
        `PlutusTx.applyCode` PlutusTx.liftCode ip
        `PlutusTx.applyCode` PlutusTx.liftCode tt
        )
        $$(PlutusTx.compile [|| wrap ||])
     where
         wrap = mkUntypedValidator @HodlState @HodlInput

-- type Client = SM.StateMachineClient HodlState HodlInput

mkInstance :: PrereqParams -> InitParams -> SM.ThreadToken -> SM.StateMachineInstance HodlState HodlInput
mkInstance pp ip tt = SM.StateMachineInstance (stateMachine pp ip tt) (tValidator pp ip tt)

validator :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Validator
validator pp ip tt = Scripts.validatorScript  (tValidator pp ip tt)
