{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards    #-}

module GuchiTx.Hodl.OffTypes where

import Data.Aeson             ( FromJSON, ToJSON )
import GHC.Generics           ( Generic )

import Ledger                 qualified as L
import Ledger.Ada             qualified as Ada

import GuchiTx.Common.OnTypes
    ( Collateral (Collateral)
    , LockedAmount (LockedAmount)
    , LockedCs (LockedCs)
    )
import GuchiTx.Hodl.OnTypes   ( InitParams (..) )

data InitCParams
  = InitCParams
      { icpLockedCs   :: !L.CurrencySymbol
      , icpAmount     :: !Integer
      , icpCollateral :: !Integer
      , icpReturnPkh  :: !L.PaymentPubKeyHash
      , icpReturnSkh  :: !(Maybe L.StakePubKeyHash)
      , icpParty      :: !L.PaymentPubKeyHash
      , icpUtxo       :: !(L.TxOutRef, L.ChainIndexTxOut)
      }
  deriving (Eq, FromJSON, Generic, Show, ToJSON)


ipFromIcp :: InitCParams -> InitParams
ipFromIcp InitCParams{..} =
  InitParams
    { ipLockedCs = LockedCs icpLockedCs
    , ipAmount = LockedAmount icpAmount
    , ipCollateral = Collateral (Ada.fromValue . Ada.lovelaceValueOf $ icpCollateral)
    , ipReturnPkh = icpReturnPkh
    , ipReturnSkh = icpReturnSkh
    , ipParty = icpParty
    }

