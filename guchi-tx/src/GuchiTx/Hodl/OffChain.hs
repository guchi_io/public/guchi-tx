{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DeriveAnyClass  #-}
{-# LANGUAGE DerivingVia     #-}
{-# LANGUAGE RecordWildCards #-}

module GuchiTx.Hodl.OffChain where

import Data.Aeson              ( FromJSON, ToJSON )
import Data.Text               ( pack )

import PlutusPrelude           ( Generic )

import Ledger                  qualified as L


import GuchiTx.StateMachine    ( MachineTransition (mtMachineWithState) )
import GuchiTx.StateMachine    qualified as SM


import GuchiTx.Types
    ( CurrentState (CurrentState, csMachineParams, csStateParams, csTxOutRef)
    , Request (..)
    , Response (..)
    , TxError (TransitionError, UtxError)
    )


import GuchiTx.Common.OffChain
import GuchiTx.Common.OnTypes
import GuchiTx.Hodl.OffTypes
import GuchiTx.Hodl.OnTypes
import GuchiTx.Hodl.Validator
import Ledger.Typed.Scripts    ( validatorAddress )

address :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Address
address pp ip tt = validatorAddress $ tValidator pp ip tt

data HodlParams
  = HodlParams
      { hpPrereqParams :: PrereqParams
      , hpInitParams   :: InitParams
      , hpThreadToken  :: SM.ThreadToken
      }
  deriving (FromJSON, Generic, Show, ToJSON)


type HodlCurrent = CurrentState HodlParams HodlState

type HodlRequest = Request HodlParams HodlState HodlInput

type HodlResponse = Response HodlParams HodlState

mkMachineWithMStake :: HodlParams -> SM.MachineWithMStake HodlState HodlInput
mkMachineWithMStake (HodlParams pp ip tt) = SM.MachineWithMStake (mkInstance pp ip tt) (fmap skhToCredential $ ppSkh pp)

mkMachineWithUtxo :: L.TxOutRef -> SM.State HodlState -> HodlParams -> SM.MachineWithUtxo HodlState HodlInput
mkMachineWithUtxo tout state params = SM.mwmsAddUtxo tout state (mkMachineWithMStake params)

----------------------------------------------------------------------------------------------------

initTx :: InitCParams -> PrereqParams -> Either TxError HodlResponse
initTx icp@InitCParams{..} pp =
    let
        ip = ipFromIcp icp
        tt = SM.mkThreadToken (fst icpUtxo)
        params = HodlParams pp ip tt
        val = initValue pp ip
        trans = SM.initMachine icpUtxo (mkMachineWithMStake params ) Initted val Nothing
        utxM = SM.transTx trans
    in case utxM of
            Left e    -> Left (UtxError $ pack $ show e)
            Right utx -> Right $ Response params (SM.State Initted val) (modTxOut (modStakeHack isScript (ppSkh pp)) utx)

stepTx :: HodlRequest -> Either TxError HodlResponse
stepTx Request{..} =
    let CurrentState{..} = rqCurrent
        mtM = SM.mkStep (mkMachineWithUtxo csTxOutRef csStateParams csMachineParams) rqInput
        HodlParams pp _ _ = csMachineParams
    in case mtM of
           Left e -> Left  (TransitionError $ pack $ show e)
           Right mt -> do
               case SM.transTx mt of
                   Left e    -> Left (UtxError $ pack $ show e)
                   Right utx -> Right $ Response csMachineParams (SM.mwsState $ mtMachineWithState mt)  (modTxOut (modStakeHack isScript (ppSkh pp)) utx)
