{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}

{-# OPTIONS_GHC -fno-specialise #-}

module GuchiTx.Common.OnChain where


import Data.Void                        ( Void )
import GuchiTx.Common.OnTypes
import GuchiTx.StateMachine             ( State (..) )
import Ledger                           qualified as L
import Ledger.Ada                       qualified as Ada
import Ledger.Constraints               qualified as Constraints
import Ledger.Constraints.TxConstraints ( TxConstraints )
import Ledger.Interval                  qualified as Interval
import Plutus.V1.Ledger.Api             qualified as L
import Plutus.V1.Ledger.Api             qualified as Value
import PlutusTx.AssocMap                qualified as Map
import PlutusTx.Prelude
    ( Bool (False, True)
    , Eq ((==))
    , Integer
    , Maybe (Just, Nothing)
    , Ord (max)
    , abs
    , divide
    , maybe
    , mempty
    , sum
    , ($)
    , (*)
    , (+)
    , (.)
    , (<>)
    )

{-# INLINABLE toStakePkh #-}
toStakePkh :: L.Address -> Maybe L.PubKeyHash
toStakePkh (L.Address _ (Just (L.StakingHash (L.PubKeyCredential k)))) = Just k
toStakePkh _                                                           = Nothing

{-# INLINABLE  calcPFee #-}
calcPFee :: PrereqParams -> Collateral -> L.Value
calcPFee PrereqParams{ppListingFee, ppFeeCoef} (Collateral col) = Ada.lovelaceValueOf $ max (Ada.getLovelace ppListingFee) (Ada.getLovelace col * unFeeCoef ppFeeCoef `divide` 1_000_000)

-- | Taking `abs` here is probably unnecessary.
-- (Clearly) its inconsequential for well-behaved users.
-- Evil users putting negative numbers would probably lead to something else breaking

{-# INLINABLE csAmountsToValue #-}
csAmountsToValue :: L.CurrencySymbol -> [(L.TokenName, Integer)] -> L.Value
csAmountsToValue cs amts = Value.Value $ Map.singleton cs $ Map.fromList [(tn, abs amt) | (tn, amt) <- amts]

{-# INLINABLE totalCurrencySymbol #-}
totalCurrencySymbol :: L.CurrencySymbol -> L.Value -> Integer
totalCurrencySymbol cs val = maybe 0 (sum . Map.elems) $ Map.lookup cs (Value.getValue val)


{-# INLINABLE mustPayAddress #-}
mustPayAddress :: L.PaymentPubKeyHash -> Maybe L.StakePubKeyHash -> L.Value -> TxConstraints i o
mustPayAddress p (Just s) v = Constraints.mustPayToPubKeyAddress p s v
mustPayAddress p _ v        = Constraints.mustPayToPubKey p v

{-# INLINABLE mustValidateSoon #-}
mustValidateSoon :: Value.POSIXTime -> TxConstraints i o
mustValidateSoon now = Constraints.mustValidateIn (Interval.interval now (now + twentyMinutes))

{-# INLINABLE isValidStake #-}
isValidStake :: Maybe L.StakePubKeyHash -> Maybe L.PubKeyHash -> Bool
isValidStake desired actual = case (desired, actual) of
                    (Just (L.StakePubKeyHash desiredPkh), Just actualStakePkh) -> desiredPkh == actualStakePkh
                    (Just _, Nothing)     -> False
                    _                     -> True

{-# INLINABLE twentyMinutes #-}
twentyMinutes :: Value.POSIXTime
twentyMinutes = L.POSIXTime (20 * 60 * 1000)

----------------------------------------------------------------------------------------------------

{-# INLINABLE  cancelTr #-}
cancelTr :: forall state . PrereqParams -> L.PaymentPubKeyHash -> state -> Maybe (TxConstraints Void Void, State state)
cancelTr PrereqParams{..} partyPkh s = Just (constraints, newState)
    where
        constraints = mustBeSignedByParty <> mustPayPFee
        mustBeSignedByParty = Constraints.mustBeSignedBy partyPkh
        mustPayPFee = mustPayAddress ppPkh ppSkh (Ada.toValue ppCancelFee)
        newState = State s mempty

{-# INLINABLE  claimTr #-}
claimTr
    :: forall state .
    PrereqParams
    -> L.PaymentPubKeyHash
    -> L.POSIXTime
    -> L.Value
    -> state
    -> Maybe (TxConstraints Void Void, State state)
claimTr PrereqParams{..} lender deadline pFee s = Just (constraints, newState)
    where
        constraints = mustBeSignedByLender
                     <> mustBeAfterDeadline
                     <> mustPayPFee

        mustPayPFee = mustPayAddress ppPkh ppSkh pFee

        mustBeSignedByLender = Constraints.mustBeSignedBy lender
        mustBeAfterDeadline =  Constraints.mustValidateIn (Interval.from deadline)

        newState = State s mempty

