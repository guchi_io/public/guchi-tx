{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE MonoLocalBinds     #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}

-- Modified version of plutus-apps StateMachine

module GuchiTx.StateMachine
    ( -- $statemachine
      InvalidTransition (..)
    , SM.State (..)
    , SM.StateMachine (..)
    , SM.StateMachineInstance (..)
    , ThreadToken (..)
      -- * Constructing the machine instance
    , SM.mkStateMachine
    , SM.mkValidator
      -- * Machines
    , MachineTransition (..)
    , MachineWithMStake (..)
    , MachineWithState (..)
    , MachineWithUtxo (..)
      -- * Methods
    , initMachine
    , mkStep
    , mkThreadToken
    , mwmsAddUtxo
    , mwmsAddress
    , mwuAddress
    , transTx
    , updateTT
    ) where

import Data.Aeson                           ( FromJSON, ToJSON )
import Data.Map                             ( Map )
import Data.Map                             qualified as Map
import GHC.Generics                         ( Generic )

import Ledger                               ( Redeemer (Redeemer), TxOutRef, Value )
import Ledger                               qualified as L

import Ledger.Constraints
    ( MkTxError
    , ScriptLookups
    , TxConstraints
    , mkTx
    , mustIncludeDatum
    , mustMintValueWithRedeemer
    , mustPayToTheScript
    , mustSpendPubKeyOutput
    , plutusV1MintingPolicy
    )
import Ledger.Constraints.OffChain          ( UnbalancedTx )
import Ledger.Constraints.OffChain          qualified as Constraints
import Ledger.Constraints.TxConstraints
    ( ScriptInputConstraint (ScriptInputConstraint, icRedeemer, icTxOutRef)
    , ScriptOutputConstraint (..)
    , txOwnInputs
    , txOwnOutputs
    )
import Ledger.Credential                    ( Credential (..), StakingCredential )
import Ledger.Tx                            ( ChainIndexTxOut (..) )
import Ledger.Typed.Scripts                 qualified as Scripts

import GuchiTx.StateMachine.MintingPolarity ( MintingPolarity (Burn, Mint) )
import GuchiTx.StateMachine.OnChain
    ( State (..)
    , StateMachine (StateMachine, smFinal, smThreadToken, smTransition)
    , StateMachineInstance (StateMachineInstance, stateMachine, typedValidator)
    )
import GuchiTx.StateMachine.OnChain         qualified as SM
import GuchiTx.StateMachine.ThreadToken     ( ThreadToken (ThreadToken), curPolicy, ttOutRef )
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
-- import Plutus.ChainIndex (ChainIndexTx (_citxInputs))
import Plutus.Script.Utils.Scripts
    ( Language (PlutusV1)
    , Versioned (Versioned, unversioned)
    , datumHash
    )
import Plutus.Script.Utils.V1.Scripts       ( scriptCurrencySymbol, validatorHash )
import Plutus.Script.Utils.V1.Typed.Scripts ( validatorScript )
import PlutusTx                             ( toBuiltinData )
import PlutusTx qualified
import PlutusTx.Monoid                      ( inv )


-- | Determines a machine's address
data MachineWithMStake s i
  = MachineWithMStake
      { mwmsMachineInst :: StateMachineInstance s i
      , mwmsStake       :: Maybe StakingCredential
      }

-- | Determines a machine's state but before a Utxo is known
-- Required when handling unbalanced txs
data MachineWithState s i
  = MachineWithState
      { mwsMachineInst :: StateMachineInstance s i
      , mwsStake       :: Maybe StakingCredential
      , mwsState       :: State s
        -- ^ State and value
      }
    -- deriving (Show, Generic)
    -- deriving (FromJSON, ToJSON)

-- | Entirely determines a machine's state as it exists on-chain
-- Required when handling unbalanced txs
data MachineWithUtxo s i
  = MachineWithUtxo
      { mwuMachineInst :: StateMachineInstance s i
      , mwuStake       :: Maybe StakingCredential
      , mwuState       :: State s
      , mwuTxOutRef    :: TxOutRef
        -- ^ The utxo currently holding the tt and thus also the state datum hash.
        -- By assumption the value held at the utxo is `(1 TT + stateValue)
        -- and it resides at a script address,. What if staked?!
      }
    -- deriving (Show, Generic)
    -- deriving (FromJSON, ToJSON)

-- | Constraints & lookups needed to transition a state machine instance
data MachineTransition state input
  = MachineTransition
      { mtConstraints      :: TxConstraints input state
      , mtLookups          :: ScriptLookups (StateMachine state input)
      , mtMachineWithState :: MachineWithState state input
      }


data InvalidTransition s i
  = InvalidTransition
      { tfState :: Maybe (State s)
        -- ^ Current state. 'Nothing' indicates that there is no current state.
      , tfInput :: i
        -- ^ Transition that was attempted but failed
      }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (FromJSON, ToJSON)

mkThreadToken :: TxOutRef -> ThreadToken
mkThreadToken txOutRef = ThreadToken txOutRef (scriptCurrencySymbol (curPolicy txOutRef))

mwmsAddress :: MachineWithMStake s i  -> L.Address
mwmsAddress mwms = L.Address (ScriptCredential . Scripts.validatorHash . typedValidator . mwmsMachineInst $ mwms) (mwmsStake mwms)

mwmsAddUtxo :: TxOutRef -> State s -> MachineWithMStake s i -> MachineWithUtxo s i
mwmsAddUtxo tor state MachineWithMStake{..} = MachineWithUtxo mwmsMachineInst mwmsStake state tor

mwuUnState :: MachineWithUtxo s i -> MachineWithMStake s i
mwuUnState MachineWithUtxo{mwuMachineInst=mi, mwuStake=stake} = MachineWithMStake mi stake

mwuAddress :: MachineWithUtxo s i  -> L.Address
mwuAddress = mwmsAddress . mwuUnState

mwuUtxo :: PlutusTx.ToData s => MachineWithUtxo s i -> Map TxOutRef ChainIndexTxOut
mwuUtxo mwu = Map.singleton (mwuTxOutRef mwu) ci
    where
          ci :: ChainIndexTxOut
          ci = ScriptChainIndexTxOut ciAddress ciValue (ciDatHash, Just ciDatum) Nothing  (ciValHash, Just ciValidator)
          ciAddress = mwuAddress mwu
          ciValidator = Versioned (validatorScript . typedValidator . mwuMachineInst $ mwu) PlutusV1
          ciValHash = validatorHash $ unversioned ciValidator
          ciDatum = L.Datum . toBuiltinData . stateData . mwuState $ mwu
          ciDatHash = datumHash ciDatum
          ciValue = (SM.threadTokenValueOrZero .  mwuMachineInst $ mwu) <> (stateValue . mwuState $ mwu)

-- | On make step we'll have a new state but won't yet know the next utxo host

bumpMachineState :: State state -> MachineWithUtxo state input -> MachineWithState state input
bumpMachineState s MachineWithUtxo{..} = MachineWithState mwuMachineInst mwuStake s

addMachineState :: State state -> MachineWithMStake state input -> MachineWithState state input
addMachineState s MachineWithMStake{..} = MachineWithState mwmsMachineInst mwmsStake s


updateTT :: ThreadToken -> SM.StateMachineInstance s i -> SM.StateMachineInstance s i
updateTT tt (StateMachineInstance sm tv) = StateMachineInstance (SM.StateMachine (SM.smTransition sm) (SM.smFinal sm) (SM.smCheck sm) (Just tt)) tv

-- | Helper function in case of no extra constraints or lookups, build unblanaced tx
transTx :: forall state input . (PlutusTx.ToData state, PlutusTx.FromData state, PlutusTx.ToData input) => MachineTransition state input -> Either MkTxError UnbalancedTx
transTx mt = mkTx (mtLookups mt) (mtConstraints mt)

initMachine::
    (TxOutRef, ChainIndexTxOut)
    -> MachineWithMStake state input
    -> state
    -> Value
    -> Maybe L.Datum  -- FIXME: Decide whether or not put parameters into datm, and then datum onchain. Probably just wait to V2.
    -> MachineTransition state input
initMachine utxo mwms@MachineWithMStake{..} initialState initialValue datumM = do
    let tt = mkThreadToken (fst utxo)
    let inst = updateTT tt mwmsMachineInst
    let ttVal = SM.threadTokenValueOrZero inst
    let StateMachineInstance{stateMachine, typedValidator} = inst
    let s = State initialState initialValue

        constraints =
            mustPayToTheScript initialState (initialValue <> ttVal)
            <> foldMap ttConstraints (smThreadToken stateMachine)
            <> maybe mempty mustIncludeDatum datumM
        red = L.Redeemer (PlutusTx.toBuiltinData (Scripts.validatorHash typedValidator, Mint))
        ttConstraints ThreadToken{ttOutRef} =
            mustMintValueWithRedeemer red ttVal
            <> mustSpendPubKeyOutput ttOutRef
        lookups = Constraints.typedValidatorLookups typedValidator
            <> foldMap (plutusV1MintingPolicy . curPolicy . ttOutRef) (smThreadToken stateMachine)
            <> Constraints.unspentOutputs (uncurry Map.singleton utxo)
    MachineTransition
        { mtConstraints = constraints
        , mtLookups = lookups
        , mtMachineWithState = addMachineState s mwms
        }

mkStep ::
    forall state input .
    ( PlutusTx.ToData state
    )
    => MachineWithUtxo state input
    -> input
    -> Either (InvalidTransition state input) (MachineTransition state input)
mkStep mwu input = do
    let MachineWithUtxo{ mwuMachineInst=smInst@StateMachineInstance{stateMachine, typedValidator}
                      , mwuState=oldState
                      , mwuTxOutRef=oldTxOutRef
                } = mwu
        StateMachine{smTransition} = stateMachine
    let inputConstraints = [ScriptInputConstraint{icRedeemer=input, icTxOutRef = oldTxOutRef }]
    let utxo = mwuUtxo mwu
    case smTransition oldState input of
        Just (newConstraints, newState)  ->
            let isFinal = smFinal stateMachine (stateData newState)
                lookups =
                    Constraints.typedValidatorLookups typedValidator
                    <> Constraints.unspentOutputs utxo
                    <> if isFinal then foldMap (plutusV1MintingPolicy . curPolicy . ttOutRef) (smThreadToken stateMachine) else mempty
                red = Redeemer (PlutusTx.toBuiltinData (Scripts.validatorHash typedValidator, Burn))
                unmint = if isFinal then mustMintValueWithRedeemer red (inv $ SM.threadTokenValueOrZero smInst) else mempty
                outputConstraints =
                    [ ScriptOutputConstraint
                        { ocDatum = stateData newState
                          -- Add the thread token value back to the output
                        , ocValue = stateValue newState <> SM.threadTokenValueOrZero smInst
                        , ocReferenceScriptHash = Nothing
                        }
                    | not isFinal ]
            in Right
                $ MachineTransition
                    { mtConstraints =
                        (newConstraints <> unmint)
                            { txOwnInputs = inputConstraints
                            , txOwnOutputs = outputConstraints
                            }
                    , mtLookups = lookups
                    , mtMachineWithState = bumpMachineState newState mwu
                    }
        Nothing -> Left $ InvalidTransition (Just oldState) input
