{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE TemplateHaskell    #-}

{-# OPTIONS_GHC -fno-specialise #-}

module GuchiTx.Borrow.OnTypes where

import Data.Aeson                   ( FromJSON, ToJSON )
import GHC.Generics                 ( Generic )
import PlutusTx qualified
import PlutusTx.Prelude             ( Bool (..), Eq, Integer, Maybe, (&&), (==) )
import Prelude                      qualified as Haskell

import Ledger                       qualified as L

import GuchiTx.StateMachine.OnChain ( StateMachine (..) )

import GuchiTx.Common.OnTypes



data InitParams
  = InitParams
      { ipLockedCs   :: LockedCs
      , ipAmount     :: LockedAmount
      , ipDuration   :: L.POSIXTime
      , ipCollateral :: Collateral
      , ipLenderFee  :: LenderFee
      , ipReturnPkh  :: L.PaymentPubKeyHash
      , ipReturnSkh  :: Maybe L.StakePubKeyHash
      , ipParty      :: L.PaymentPubKeyHash
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''InitParams

-- | STATE

data EngagedState
  = EngagedState
      { esCounterParty :: L.PaymentPubKeyHash
      , esNow          :: L.POSIXTime
      , esReturnPkh    :: L.PaymentPubKeyHash
      , esReturnSkh    :: Maybe L.StakePubKeyHash
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''EngagedState
PlutusTx.unstableMakeIsData ''EngagedState

instance Eq EngagedState where
    {-# INLINABLE (==) #-}
    EngagedState a b c d == EngagedState x y z w = a == x && b == y && c == z && d == w

-- | The states of the guchi state machine.
data BorrowState
  = Initted
  | Engaged EngagedState
  | Canceled
  | Returned
  | Claimed
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

instance Eq BorrowState where
    {-# INLINABLE (==) #-}
    Initted == Initted        = True
    Engaged ep == Engaged ep' = ep == ep'
    Canceled == Canceled      = True
    Returned == Returned      = True
    Claimed == Claimed        = True
    _ == _                    = False

PlutusTx.makeLift ''BorrowState
PlutusTx.unstableMakeIsData ''BorrowState

-- END OF STATE

-- TRANSITIONS

data EngageParams
  = EngageParams
      { epCounterParty :: L.PaymentPubKeyHash
        -- ^ Bob's pkh
      , epNow          :: L.POSIXTime
        -- ^ to be within 3 hours of validators 'now'.
      , epAmounts      :: [(L.TokenName, Integer)]
      , epReturnPkh    :: L.PaymentPubKeyHash
      , epReturnSkh    :: Maybe L.StakePubKeyHash
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''EngageParams
PlutusTx.unstableMakeIsData ''EngageParams

data ReturnParams
  = ReturnParams
      { rpAmounts :: [(L.TokenName, Integer)]
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''ReturnParams
PlutusTx.unstableMakeIsData ''ReturnParams

data BorrowInput
  = Engage EngageParams
  | Return ReturnParams
  | Cancel
  | Claim
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''BorrowInput
PlutusTx.unstableMakeIsData ''BorrowInput

type BorrowMachine = StateMachine BorrowState BorrowInput

