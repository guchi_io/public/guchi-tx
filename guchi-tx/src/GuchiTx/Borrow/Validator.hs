{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeApplications   #-}

module GuchiTx.Borrow.Validator where

import PlutusTx qualified
import PlutusTx.Prelude
    ( Bool (False, True)
    , Maybe (Just, Nothing)
    , id
    , maybe
    , mempty
    , traceError
    , traceIfFalse
    , ($)
    , (&&)
    , (*)
    , (+)
    , (.)
    , (<>)
    , (==)
    , (>)
    )

import Ledger                               qualified as L
import Ledger.Ada                           qualified as Ada
import Ledger.Typed.Scripts                 qualified as Scripts

import Ledger.Constraints                   qualified as Constraints
import Ledger.Constraints.TxConstraints     ( TxConstraints )
import Ledger.Interval                      qualified as Interval
import Ledger.Value                         ( Value )

import GuchiTx.StateMachine.OnChain         ( State (..), StateMachine (..) )
import GuchiTx.StateMachine.OnChain         qualified as SM
    ( StateMachine (StateMachine)
    , StateMachineInstance (StateMachineInstance)
    , mkValidator
    )
import GuchiTx.StateMachine.ThreadToken     qualified as SM ( ThreadToken )


import Data.Void                            ( Void )

import Ledger.Value                         qualified as Value

import GuchiTx.Borrow.OnTypes
import GuchiTx.Common.OnChain
import GuchiTx.Common.OnTypes
import Plutus.Script.Utils.V1.Typed.Scripts ( mkUntypedValidator, validatorAddress )

-------------------------------------------------------------

engageTr
    :: PrereqParams
    -> InitParams
    -> EngageParams
    -> Value
    -> Maybe (TxConstraints Void Void, State BorrowState)
engageTr pp@PrereqParams{..} InitParams{..} EngageParams{..} _cVal = if isValidAssets then Just (constraints, newState) else Nothing
    where
        isValidAssets = totalCurrencySymbol (unLockedCs ipLockedCs) borrowedVal == unLockedAmount ipAmount

        constraints = mustPayBorrowerAssets
                   <> mustValidateSoon epNow

        mustPayBorrowerAssets = mustPayAddress ipReturnPkh ipReturnSkh borrowedVal

        borrowedVal = csAmountsToValue (unLockedCs ipLockedCs) epAmounts <> Ada.lovelaceValueOf ppMinAdaBuffer
        pFee = calcPFee pp ipCollateral
        lockedCollateral = Ada.toValue (unCollateral ipCollateral + Ada.lovelaceOf ppMinAdaBuffer) <> pFee
        newState = State (Engaged $ EngagedState { esCounterParty = epCounterParty , esNow = epNow , esReturnPkh = epReturnPkh, esReturnSkh = epReturnSkh }) lockedCollateral


returnTr
    :: PrereqParams
    -> InitParams
    -> ReturnParams
    -> EngagedState
    -> Maybe (TxConstraints Void Void, State BorrowState)
returnTr pp@PrereqParams{..} InitParams{..} rp EngagedState{..} = if isValidAssets then Just (constraints, newState) else Nothing
    where
        isValidAssets = totalCurrencySymbol (unLockedCs ipLockedCs) obligationVal == unLockedAmount ipAmount

        constraints = mustBeSignedByBorrower
                  <> mustPayLenderObligation
                  <> mustBeBeforeDeadline
                  <> mustPayPFee

        mustPayPFee = mustPayAddress ppPkh ppSkh pFee
        mustBeSignedByBorrower = Constraints.mustBeSignedBy ipParty
        mustPayLenderObligation = mustPayAddress esReturnPkh esReturnSkh obligationVal
        mustBeBeforeDeadline =  Constraints.mustValidateIn (Interval.to $ ipDuration + esNow )

        pFee = calcPFee pp ipCollateral
        obligationVal = csAmountsToValue (unLockedCs ipLockedCs) (rpAmounts rp) <> Ada.lovelaceValueOf ppMinAdaBuffer
        newState = State Returned mempty

{-# INLINABLE transition #-}
transition
    :: PrereqParams
    -> InitParams
    -> State BorrowState
    -> BorrowInput
    -> Maybe (TxConstraints Void Void, State BorrowState)
transition pp ip (State cState cVal) tr =
    case (tr, cState) of
         (Cancel, Initted)       -> cancelTr pp (ipParty ip) Canceled
         (Engage ep, Initted)    -> engageTr pp ip ep cVal
         (Return rp, Engaged es) -> returnTr pp ip rp es
         (Claim, Engaged es)     -> claimTr  pp (esCounterParty es) (esNow es + ipDuration ip) (calcPFee pp $ ipCollateral ip) Claimed
         _                       -> Nothing


-- END OF TRANSITION FUNCTIONS

-- Check the SM is good before running further
{-# INLINEABLE initValue #-}
initValue :: PrereqParams -> InitParams -> Value
initValue pp@PrereqParams{..} InitParams{..} = Ada.toValue (unCollateral ipCollateral + unLenderFee ipLenderFee + Ada.lovelaceOf ppMinAdaBuffer) + calcPFee pp ipCollateral

{-# INLINABLE check #-}
check :: PrereqParams -> InitParams -> BorrowState -> BorrowInput -> L.ScriptContext -> Bool
check pp@PrereqParams{..} ip@InitParams{..} _s (Engage _sp) ctx =
    traceIfFalse "C0" (isValidStake ppSkh ownStakePkh)
    && traceIfFalse "C2" isValidAda
    && traceIfFalse "C3" isValidDuration
        where
            ownStakePkh = toStakePkh . L.txOutAddress . L.txInInfoResolved $ ownInput
            isValidAda = Value.adaOnlyValue ownVal == initValue pp ip
            isValidDuration = L.getPOSIXTime ipDuration  > 3 * 60 * 60 * 1000  -- 3 hours

            ownInput :: L.TxInInfo
            ownInput = maybe (traceError "C10" {-"Can't find validation input"-}) id (L.findOwnInput ctx)
            ownVal = (L.txOutValue . L.txInInfoResolved) ownInput
check _ _ _ _ _ = True

isFinal :: BorrowState -> Bool
isFinal Canceled = True
isFinal Returned = True
isFinal Claimed  = True
isFinal _        = False

-- BOILERPLATE

{-# INLINABLE stateMachine #-}
stateMachine :: PrereqParams -> InitParams -> SM.ThreadToken-> BorrowMachine
stateMachine pp ip tt =
    SM.StateMachine { smTransition = transition pp ip
                    , smCheck = check pp ip
                    , smThreadToken = Just tt
                    , smFinal = isFinal
                    }

{-# INLINABLE mkValidator #-}
mkValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> BorrowState -> BorrowInput -> L.ScriptContext -> Bool
mkValidator pp ip tt = SM.mkValidator (stateMachine pp ip tt)

tValidator :: PrereqParams -> InitParams -> SM.ThreadToken -> Scripts.TypedValidator BorrowMachine
tValidator pp ip tt = Scripts.mkTypedValidator @BorrowMachine
        ($$(PlutusTx.compile [|| mkValidator ||])
        `PlutusTx.applyCode` PlutusTx.liftCode pp
        `PlutusTx.applyCode` PlutusTx.liftCode ip
        `PlutusTx.applyCode` PlutusTx.liftCode tt
        )
        $$(PlutusTx.compile [|| wrap ||])
     where
         wrap = mkUntypedValidator @BorrowState @BorrowInput

-- type Client = SM.StateMachineClient BorrowState Input

mkInstance :: PrereqParams -> InitParams -> SM.ThreadToken -> SM.StateMachineInstance BorrowState BorrowInput
mkInstance pp ip tt = SM.StateMachineInstance (stateMachine pp ip tt) (tValidator pp ip tt)

validator :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Validator
validator pp ip tt = Scripts.validatorScript  (tValidator pp ip tt)

address :: PrereqParams -> InitParams -> SM.ThreadToken ->  L.Address
address pp ip tt = validatorAddress $ tValidator pp ip tt
