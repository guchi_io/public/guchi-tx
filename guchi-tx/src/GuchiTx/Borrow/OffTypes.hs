{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards    #-}

module GuchiTx.Borrow.OffTypes where

import Data.Aeson               ( FromJSON, ToJSON )
import GHC.Generics             ( Generic )

import Ledger                   qualified as L
import Ledger.Ada               qualified as Ada

import GuchiTx.Borrow.OnTypes
import GuchiTx.Borrow.Validator ( mkInstance )
import GuchiTx.Common.OffChain  ( skhToCredential )
import GuchiTx.Common.OnTypes
import GuchiTx.StateMachine     qualified as SM
import GuchiTx.Types

data InitCParams
  = InitCParams
      { icpCurrencySymbol :: !L.CurrencySymbol
      , icpAmount         :: !Integer
      , icpDuration       :: !L.POSIXTime
      , icpCollateral     :: !Integer
      , icpFee            :: !Integer
      , icpReturnPkh      :: !L.PaymentPubKeyHash
      , icpReturnSkh      :: !(Maybe L.StakePubKeyHash)
      , icpPkh            :: !L.PaymentPubKeyHash
      , icpUtxo           :: !(L.TxOutRef, L.ChainIndexTxOut)
      }
  deriving (Eq, FromJSON, Generic, Show, ToJSON)



data BorrowParams
  = BorrowParams
      { bpPrereqPramas :: PrereqParams
      , bpInitParams   :: InitParams
      , bpThreadToken  :: SM.ThreadToken
      }
  deriving (FromJSON, Generic, Show, ToJSON)

type BorrowCurrent = CurrentState BorrowParams BorrowState

type BorrowRequest = Request BorrowParams BorrowState BorrowInput

type BorrowResponse = Response BorrowParams BorrowState

mkMachineWithMStake :: BorrowParams -> SM.MachineWithMStake BorrowState BorrowInput
mkMachineWithMStake (BorrowParams pp ip tt) = SM.MachineWithMStake (mkInstance pp ip tt) (fmap skhToCredential $ ppSkh pp)

mkMachineWithUtxo :: L.TxOutRef -> SM.State BorrowState -> BorrowParams -> SM.MachineWithUtxo BorrowState BorrowInput
mkMachineWithUtxo tor s bp = SM.mwmsAddUtxo tor s (mkMachineWithMStake bp)

ipFromIcp :: InitCParams -> InitParams
ipFromIcp InitCParams{..} = InitParams { ipLockedCs = LockedCs icpCurrencySymbol
                           , ipAmount = LockedAmount icpAmount
                           , ipDuration = icpDuration
                           , ipCollateral = Collateral $ Ada.lovelaceOf icpCollateral
                           , ipLenderFee = LenderFee $ Ada.lovelaceOf icpFee
                           , ipReturnPkh = icpReturnPkh
                           , ipReturnSkh = icpReturnSkh
                           , ipParty = icpPkh
                           }
