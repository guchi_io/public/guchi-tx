{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DerivingVia        #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}


module Spec.GuchiTx.Lend.OffChain where

import Data.Default                        ( def )
import Data.Monoid                         ( Last (Last) )

import Test.Tasty                          ( TestTree, testGroup )

import PlutusPrelude                       ( (^.) )

import Ledger.Value                        ( Value )

import Control.Monad                       ( void )
import Ledger.Ada                          qualified as Ada
import Plutus.Contract.Test
    ( TracePredicate
    , changeInitialWalletValue
    , checkPredicate
    , defaultCheckOptions
    , emulatorConfig
    , valueAtAddress
    , w1
    , w2
    , w3
    , w4
    , walletFundsChange
    , (.&&.)
    )
import Plutus.Contract.Test                qualified as Test
import Plutus.Trace                        ( runEmulatorTraceIO' )
import Plutus.Trace                        qualified as E
import PlutusTx.Monoid                     ( inv )
import Wallet.Emulator.Wallet              qualified as W

import Ledger.Ada                          ( Ada (getLovelace) )

import GuchiTx.Common.OnChain              ( calcPFee )
import GuchiTx.Common.OnTypes
import GuchiTx.Lend.OnTypes
import Spec.GuchiTx.Lend.OffChain.Contract
import Spec.Utils.Const

runSimpleComplete :: IO ()
runSimpleComplete = runEmulatorTraceIO' def (setupOptions ^. emulatorConfig ) simpleCompleteX

--- START simple complete ---

simpleCompleteP :: TracePredicate
simpleCompleteP =
    walletFundsChange w1 (feeA <> marsToken <> inv larsToken )
    .&&. walletFundsChange w2 (sellPrice <> inv (feeA <> feeP <> buyPrice))
    .&&. walletFundsChange w3 (buyPrice <> inv marsToken)
    .&&. walletFundsChange w4 (larsToken <> inv sellPrice)
    .&&. valueAtAddress testnetAddress ((==) feeP)

sellPrice :: Value
sellPrice = Ada.adaValueOf 100

collateral :: Collateral
collateral = Collateral $ Ada.fromValue $ Ada.adaValueOf 150

buyPrice :: Value
buyPrice = Ada.adaValueOf 50

feeA :: Value
feeA = Ada.adaValueOf 10

feeP :: Value
feeP = calcPFee examplePP collateral

setupOptions :: Test.CheckOptions
setupOptions =
    changeInitialWalletValue (W.knownWallet 1) (larsToken <>)
    $ changeInitialWalletValue (W.knownWallet 2) (Ada.adaValueOf 1000 <>)
    $ changeInitialWalletValue (W.knownWallet 3) (marsToken <>)
    $ changeInitialWalletValue (W.knownWallet 4) (Ada.adaValueOf 1000 <>)
    $ changeInitialWalletValue (W.knownWallet 5) (mempty <>)
    defaultCheckOptions

simpleCompleteTest :: TestTree
simpleCompleteTest = Test.checkPredicateOptions setupOptions "simpleComplete" simpleCompleteP simpleCompleteX

simpleCompleteX :: E.EmulatorTrace ()
simpleCompleteX = do
    let iw = InitW
            { iwDuration = 7 * 24 * 60 * 60 * 1000
            , iwCollateral = (getLovelace . unCollateral) collateral
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    let rp = ReturnParams
            { rpAmounts = [(marsTn, 1)]
            }
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep)
            void $ E.waitNSlots 2
            void $ E.payToWallet w4 w2 (sellPrice <> minAda')
            void $ E.payToWallet w2 w4 (larsToken <> minAda')
            void $ E.waitNSlots 2
            void $ E.payToWallet w2 w3 (buyPrice <> minAda')
            void $ E.payToWallet w3 w2 (marsToken <> minAda')
            void $ E.waitNSlots 2
            ob2 <- E.observableState h2
            case ob2 of
                Last (Just res2) -> do
                    _h2 <- E.activateContractWallet w2 $ stepC res2 (Return rp)
                    void $ E.waitNSlots 2
                _ -> void  $ E.waitNSlots 0
        _ -> void $ E.waitNSlots 0

--- END simple complete ---

--- START cancel ---

cancelP :: TracePredicate
cancelP =
    walletFundsChange w1 (inv cancelFee )
    .&&. valueAtAddress testnetAddress ((==) cancelFee)

cancelFee :: Value
cancelFee = Ada.toValue (ppCancelFee examplePP)

cancelTest :: TestTree
cancelTest = Test.checkPredicateOptions setupOptions "cancel" cancelP cancelX

cancelX :: E.EmulatorTrace ()
cancelX = do
    let iw = InitW
            { iwDuration = 7 * 24 * 60 * 60 * 1000
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            _h2 <- E.activateContractWallet w1 $ stepC res1 Cancel
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END cancel ---

--- START cancel malicious ---

cancelMaliciousP :: TracePredicate
cancelMaliciousP =
    walletFundsChange w1 (inv cancelFee )
    .&&. valueAtAddress testnetAddress ((==) cancelFee)

cancelMaliciousTest :: TestTree
cancelMaliciousTest = Test.checkPredicateOptions setupOptions "cancelMalicious" cancelMaliciousP cancelMaliciousX

cancelMaliciousX :: E.EmulatorTrace ()
cancelMaliciousX = do
    let iw = InitW
            { iwDuration = 7 * 24 * 60 * 60 * 1000
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    void $ E.waitNSlots 1
    case ob1 of
        Last (Just res1) -> do
            _h2 <- E.activateContractWallet w2 $ stepC res1 Cancel -- Should fail
            void $ E.waitNSlots 2
            _h2 <- E.activateContractWallet w1 $ stepC res1 Cancel -- Should Succeed
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END cancel malicious ---

--- START claim ---

claimP :: TracePredicate
claimP = let
        val1 = (feeA <> (Ada.toValue $ unCollateral collateral) <> inv larsToken )
           in
    walletFundsChange w1 val1
    .&&. walletFundsChange w2 (inv (val1 <> feeP))
    .&&. valueAtAddress testnetAddress ((==) feeP)

claimTest :: TestTree
claimTest = Test.checkPredicateOptions setupOptions "claim" claimP claimX

claimX :: E.EmulatorTrace ()
claimX = do
    let iw = InitW
            { iwDuration = 3 * 60 * 60 * 1000  + 1
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    -- let assets = larsToken
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep)
            void $ E.waitNSlots 2
            ob2 <- E.observableState h2
            case ob2 of
                Last (Just res2) -> do
                    void $ E.waitNSlots (3 * 60 * 60 + 2)
                    _h2 <- E.activateContractWallet w1 $ stepC res2 Claim
                    void $ E.waitNSlots 3
                _ -> void $ E.waitNSlots 0
        _ -> void $ E.waitNSlots 0

--- END claim ---

--- START claim early ---

claimEarlyP :: TracePredicate
claimEarlyP =
    walletFundsChange w1 feeA
    .&&. walletFundsChange w2 (inv (feeA <> feeP))
    .&&. valueAtAddress testnetAddress ((==) feeP)

claimEarlyTest :: TestTree
claimEarlyTest = Test.checkPredicateOptions setupOptions "claimEarly" claimEarlyP claimEarlyX

claimEarlyX :: E.EmulatorTrace ()
claimEarlyX = do
    let iw = InitW
            { iwDuration = 3 * 60 * 60 * 1000  + 1
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    let rp = ReturnParams
            { rpAmounts = [(larsTn, 1)]
            }
    -- let assets = larsToken
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep)
            void $ E.waitNSlots 2
            ob2 <- E.observableState h2
            case ob2 of
                Last (Just res2) -> do
                    _h2 <- E.activateContractWallet w1 $ stepC res2 Claim -- Should NOT make it to chain
                    void $ E.waitNSlots 3
                    _h2 <- E.activateContractWallet w2 $ stepC res2 (Return rp) -- Should succeed
                    void $ E.waitNSlots 3
                _ -> void $ E.waitNSlots 0
        _ -> void $ E.waitNSlots 0

--- END claim early ---

--- START claim malicious ---

claimMaliciousP :: TracePredicate
claimMaliciousP = let
        val1 = (feeA <> (Ada.toValue $ unCollateral collateral) <> inv larsToken )
           in
    walletFundsChange w1 val1
    .&&. walletFundsChange w2 (inv (val1 <> feeP))
    .&&. valueAtAddress testnetAddress ((==) feeP)

claimMaliciousTest :: TestTree
claimMaliciousTest = Test.checkPredicateOptions setupOptions "claimMalicious" claimMaliciousP claimMaliciousX

claimMaliciousX :: E.EmulatorTrace ()
claimMaliciousX = do
    let iw = InitW
            { iwDuration = 3 * 60 * 60 * 1000  + 1
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    -- let assets = larsToken
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep)
            void $ E.waitNSlots 2
            ob2 <- E.observableState h2
            case ob2 of
                Last (Just res2) -> do
                    void $ E.waitNSlots (3 * 60 * 60 + 2)
                    _h2 <- E.activateContractWallet w3 $ stepC res2 Claim -- Should Fail
                    void $ E.waitNSlots 3
                    _h2 <- E.activateContractWallet w1 $ stepC res2 Claim -- Should Succeed
                    void $ E.waitNSlots 3
                _ -> void $ E.waitNSlots 0
        _ -> void $ E.waitNSlots 0

--- END claim malicious ---

--- START Return late ---

returnLateP :: TracePredicate
returnLateP = let
        val1 = (feeA <> (Ada.toValue $ unCollateral collateral) <> inv larsToken )
           in
    walletFundsChange w1 val1
    .&&. walletFundsChange w2 (inv (val1 <> feeP))
    .&&. valueAtAddress testnetAddress ((==) feeP)

returnLateTest :: TestTree
returnLateTest = Test.checkPredicateOptions setupOptions "returnLate" returnLateP returnLateX

returnLateX :: E.EmulatorTrace ()
returnLateX = do
    let iw = InitW
            { iwDuration = 3 * 60 * 60 * 1000  + 1
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    let rp = ReturnParams
            { rpAmounts = [(marsTn, 1)]
            }
    -- let assets = larsToken
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep)
            void $ E.waitNSlots 2
            ob2 <- E.observableState h2
            case ob2 of
                Last (Just res2) -> do
                    void $ E.waitNSlots (3 * 60 * 60 + 2)
                    _h2 <- E.activateContractWallet w1 $ stepC res2 (Return rp) -- Should fail
                    void $ E.waitNSlots 3
                    _h2 <- E.activateContractWallet w1 $ stepC res2 Claim -- Should Succeed
                    void $ E.waitNSlots 3
                _ -> void $ E.waitNSlots 0
        _ -> void $ E.waitNSlots 0

--- END Return late ---

--- START window invalid ---

windowInvalidP :: TracePredicate
windowInvalidP = walletFundsChange w1 (inv cancelFee)
        .&&. walletFundsChange w2 mempty
    .&&. valueAtAddress testnetAddress ((==) cancelFee)

windowInvalidTest :: TestTree
windowInvalidTest = Test.checkPredicateOptions setupOptions "windowInvalid" windowInvalidP windowInvalidX

windowInvalidX :: E.EmulatorTrace ()
windowInvalidX = do
    let iw = InitW
            { iwDuration = 3 * 60 * 60 * 1000 - 1000
            , iwCollateral = 150_000_000
            , iwFee = (getLovelace . Ada.fromValue) feeA
            }
    let ep = EngageParams
            { epPkh = W.mockWalletPaymentPubKeyHash w2
            , epNow = beginningOfTime
            }
    -- let assets = larsToken
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            _h2 <- E.activateContractWallet w2 $ stepC res1 (Engage ep) -- Should fail
            void $ E.waitNSlots 2
            _ <- E.activateContractWallet w1 $ stepC res1 Cancel -- Should Succeed
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END window invalid ---

--- START ZZZ ---

zzzP :: TracePredicate
zzzP = undefined

zzzX :: E.EmulatorTrace ()
zzzX = undefined

zzzTest :: TestTree
zzzTest = checkPredicate "zzzTest" zzzP zzzX

--- END ZZZ ---

tests :: TestTree
tests =
    testGroup "lend contracts"
    [ simpleCompleteTest
    , cancelTest
    , cancelMaliciousTest
    , claimTest
    , claimEarlyTest
    , returnLateTest
    , windowInvalidTest
    -- , zzzTest
    ]
