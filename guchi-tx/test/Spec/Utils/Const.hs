{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}

module Spec.Utils.Const where

import Data.Maybe               ( fromJust )

import Ledger                   qualified as L
import Ledger.Ada               qualified as Ada
import Ledger.Value             ( CurrencySymbol, TokenName, Value )
import Ledger.Value             qualified as Value

import Plutus.V1.Ledger.Address ( Address (Address) )
import Plutus.V1.Ledger.Api     ( Credential (PubKeyCredential), StakingCredential (StakingHash) )
import PlutusTx.Prelude         ( Maybe (Just), ($) )

import GuchiTx.Common.OnChain   ( toStakePkh )
import GuchiTx.Common.OnTypes   ( FeeCoef (FeeCoef), PrereqParams (PrereqParams) )


beginningOfTime :: L.POSIXTime
beginningOfTime = L.POSIXTime 1596059091000

larsCs :: CurrencySymbol
larsCs = "00000000000000000000000000000000000000000000000000000000"

larsTn :: TokenName
larsTn = "LarsIsMyHero!"

larsToken :: Value
larsToken = Value.singleton larsCs larsTn 1

marsCs :: CurrencySymbol
marsCs = "00000000000000000000000000000000000000000000000000000000"

marsTn :: TokenName
marsTn = "MarsTnToZero?"

marsToken :: Value
marsToken = Value.singleton marsCs marsTn 1

minAda' :: Value
minAda' = Ada.lovelaceValueOf 2_000_000

testnetAddress :: Address
testnetAddress = Address
  { L.addressCredential = pc
  , L.addressStakingCredential = Just (StakingHash sc)
  }
  where
        pc = PubKeyCredential "66010289247d3ed2051d0b6f7cb3b24030311a6910345f035d0d21b8"
        sc = PubKeyCredential "3aa9f2fe3d5e05030e909e5c4e8a0c62e908fa744c335e33be139163"

knownPkh :: L.PaymentPubKeyHash
knownPkh = L.PaymentPubKeyHash $ fromJust $ L.toPubKeyHash testnetAddress

knownSkh :: Maybe L.StakePubKeyHash
knownSkh = Just $ L.StakePubKeyHash $ fromJust $ toStakePkh testnetAddress

examplePP ::  PrereqParams
examplePP = PrereqParams 2_000_000 2_000_000 (FeeCoef 20_000) knownPkh knownSkh 5_000_000
