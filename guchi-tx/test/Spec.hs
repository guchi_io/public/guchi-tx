{-# LANGUAGE OverloadedStrings #-}
module Main
    ( main
    ) where

import Test.Tasty
import Test.Tasty.Hedgehog          ( HedgehogTestLimit (..) )

--import Spec.Guchi qualified
import Spec.GuchiTx.Borrow.OffChain qualified
import Spec.GuchiTx.Hodl.OffChain qualified
import Spec.GuchiTx.Lend.OffChain qualified


main :: IO ()
main = defaultMain tests

limit :: HedgehogTestLimit
limit = HedgehogTestLimit (Just 5)

tests :: TestTree
tests = localOption limit $ testGroup "guchi-tx"
    [ Spec.GuchiTx.Hodl.OffChain.tests
    , Spec.GuchiTx.Borrow.OffChain.tests
    , Spec.GuchiTx.Lend.OffChain.tests
    ]
    -- ,
    -- Spec.GuchiTx.Lend.OffChain.tests
    -- ]
