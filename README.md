# Guchi Tx 

Validators for [guchi.io](https://guchi.io/). 

## Setup env

This repo uses the plutus-apps framework.

The setup here is akin to that used in the [plutus-pioneer-program](https://github.com/input-output-hk/plutus-pioneer-program).
That is, it uses nix. 
Moreover, you need to open the nix-shell of plutus-apps checked-out at a specific commit.
To familiarize yourself with how this is done, refer to lecture 1, cohort 3 of the PPP. 

We actually use a fork of plutus-apps, which is also available on our gitlab. 
Checkout the `cabal.project` file for the precise git commit used. 
The main reason for having a fork of plutus-apps was to allow for staked script addresses, 
which was not otherwise possible.

## Validators 

There are three families of validators: "Lend", "Borrow", and "HODL". 
As far as is reasonably possible, the logic is written in a declarative fashion. 
Thus, regardless of whether or not you are Haskell/Plutus-framework fluent, 
you should be able to glean some idea of what happens at each step, 
especially if you match this against our other documentation (*citation needed*).
